# CREATE IT WEBSITE

## Description
This repository contain backend and frontend server. Built using Node Js. React Js for Frontend and Express Js for Backend.


## RUN
How to run Backend:

- $ cd createit-server
- $ git checkout development
- $ npm install
- $ touch .env
- $ nano .env (for specific purpose please contact administrator

How to run frontend:

- $ cd createit
- $ git checkout development
- $ npm install

`//edit baseURL.js in src/constants to change backend server`

- $ npm start

## ISSUES
- AI integration not yet
- Sendinblue API doesn't send email yet