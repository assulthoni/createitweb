import BriefContest from './Brief/Contest'
import ContestReview from './Brief/ContestReview'
import BriefProject from './Brief/Project'
import ProjectReview from './Brief/ProjectReview'

// import ClientContestDashboard from './ContestDashboard/Index'

import Pricing from './Pricing/Index'

import ClientProfile from './Client'
import ContestList from './ContestList'
import Payment from './Payment'
import ProjectList from './ProjectList'

export {
    BriefContest, ContestReview, BriefProject, ProjectReview,
    Pricing, ClientProfile, ContestList,
    Payment, ProjectList
}