import React, { useState, useEffect } from 'react'
import NumberFormat from 'react-number-format';
import LoadingOverlay from 'react-loading-overlay'
import { connect } from 'react-redux'
import { withRouter, Link } from 'react-router-dom'
import contestAction from '../../../Modules/Redux/Actions/Contest'
import TableSection from './TableSection';
import { Navbar, Subfooter } from '../../../Components/Index';

const Pricing = (props) => {
    const { contestID } = props.match.params
    const { contest } = props
    const [state, setState] = useState({
        choosenPackIdx: null, contestType: null, saved: false
    })


    const contestTypes = [
        { title: 'Public', price: 'Free', desc: 'Kontes tampil secara umum di website Creat It.', options: ['Siapapun bisa melihat progres kontes kamu; Creator maupun bukan', 'Lebih banyak creator yang dapat tertarik mengikuti kontes kamu'] },
        { title: 'Stealth', price: '200k', desc: 'Progress kontes kamu tidak kasat mata dan akan tampil hanya setelah kontes selesai', options: ['Hanya kamu sebagai pembuat kontes yang dapat melihat progress kontes', 'Hasil kontes akan tampil lebih unik'] },
        { title: 'Ninja', price: '500k', desc: 'Tidak akan ada yang tahu keberadaan kontes kamu. Apakah ini nyata atau hanya sebuah ilusi?', options: ['Hanya kamu sebagai pembuat kontes yang dapat melihat progress dan hasil kontes yang rahasia ini', 'Pengerjaan kontes dilakukan secara rahasia, sehingga para Creator yang mendaftar harus menandatangani perjanjian rahasia.'] }
    ]

    const pack = [
        { pricingPack: 'Paket A', price: 2000000 },
        { pricingPack: 'Paket B', price: 2000000 },
        { pricingPack: 'Paket C', price: 2000000 },
        { pricingPack: 'Paket D', price: 2000000 },
    ]

    const onSave = () => {
        setState({ ...state, saved: true })
        const payload = {
            contestType: state.contestType,
            ...pack[state.choosenPackIdx]
        }
        props.updateContest(contestID, payload)
    }

    const onContinue = () => {
        if (!state.saved) {
            const payload = {
                contestType: state.contestType,
                ...pack[state.choosenPackIdx]
            }
            props.updateContest(contestID, payload)
            props.history.replace(`/contest/dashboard/${contestID}`)
        } else {
            props.history.replace(`/contest/dashboard/${contestID}`)
        }
    }

    const onChoose = (index) => {
        setState({ ...state, choosenPackIdx: index })
    }

    const pickContestType = (type) => {
        setState({ ...state, contestType: type })
    }

    useEffect(() => {
        if (contest) {
            const index = pack.findIndex(item => item.pricingPack == contest.pricingPack)
            setState({ contestType: contest.contestType, choosenPackIdx: index != -1 ? index : null })
        }
    }, [contest])

    useEffect(() => {
        props.getContestById(contestID, props.history)
    }, [])

    return (
        <LoadingOverlay active={props.loading} spinner text="Loading please wait...">
            <Navbar />
            <div className='bg-light'>
                <div className='container py-5'>
                    <div className='row'>
                        <div className='col-md d-flex'>
                            <div className='m-auto'>
                                <img src={require('../../../Modules/images/logo.png')} width='200px' />
                                <h6 className='mt-4 text-secondary'>{contest?.name} / Brief / Ulasan / <strong>Pricing</strong></h6>
                                <h3 className='font-weight-bold text-main mb-0'>Pricing</h3>
                                <h1 className='text-main font-weight-bold'>Create Contest</h1>
                                <div className='text-secondary'>
                                    Pilih paket harga sesuai kebutuhanmu dan kantongmu. Hasil desain nanti akan seutuhnya menjadi hak milik kamu.
                                </div>
                            </div>
                        </div>
                        <div className='col-md d-flex'>
                            <img src={require('../../../Modules/images/brief-mascot.png')} width='60%' className='m-auto' />
                        </div>
                    </div>
                </div>
            </div>

            <div className='container py-5'>
                <h3 className='font-weight-bold text-secondary'>Paket Harga</h3>
                <TableSection onChoose={onChoose} choosenPackIdx={state.choosenPackIdx} />
            </div>

            <div className='bg-light py-5 text-secondary'>
                <div className='container my-3'>
                    <div className='d-flex mb-2'>
                        <div className='bg-white rounded-lg border mr-3 shadow-sm' style={{ width: '40px', height: '40px' }}></div>
                        <h2>Featured Contest ( <NumberFormat value={2456981} displayType={'text'} thousandSeparator={true} prefix={'IDR. '} /> )</h2>
                    </div>
                    <div className='ml-3' style={{ maxWidth: '600px' }}>
                        <h6 style={{ maxWidth: '500px' }}>Jadikan kontesmu lebih meriah dengan topping ini. yang memiliki keuntungan:</h6>
                        <div className='d-flex my-auto py-3'>
                            <i className='fa fa-check text-success mr-3' style={{ fontSize: '30px' }} />
                            <h6 className='my-auto'>Kontes akan berada di bagian paling atas dengan tampilan tersendiri sehingga akan menarik perhatian para creator!</h6>
                        </div>
                        <div className='d-flex my-auto pb-3'>
                            <i className='fa fa-check text-success mr-3' style={{ fontSize: '30px' }} />
                            <h6 className='my-auto'>Creator akan mendapatkan bonus dengan kamu menjadi pahlawan mereka!</h6>
                        </div>
                    </div>
                </div>
            </div>

            <div className='container py-5'>
                <h3 className='font-weight-bold text-secondary'>Tipe Kontes</h3>
                <div className='justify-content-center d-flex flex-wrap'>
                    {contestTypes.map(item => (
                        <div className={'bg-light px-5 py-4 m-3 text-secondary border-main' + (state.contestType == item.title ? '-active' : '')}
                            style={{ borderRadius: '20px', maxWidth: '500px' }}>
                            <div className='d-flex'>
                                <i className='fa mr-2 text-main my-auto fa-wine-bottle' style={{ fontSize: '30px' }} />
                                <h2 className='text-main font-weight-bold my-auto ml-3'>{item.title}</h2>
                                <h5 className='text-secondary my-auto ml-auto'>{item.price}</h5>
                            </div>
                            <div className='my-3'>
                                {item.desc}
                            </div>
                            {item.options?.map(opt => (
                                <div className='d-flex my-auto pb-3'>
                                    <i className='fa fa-check text-success mr-3' style={{ fontSize: '30px' }} />
                                    <h6 className='my-auto'>{opt}</h6>
                                </div>
                            ))}
                            <div className='d-flex'>
                                <button className='btn btn-category px-5 py-3 ml-auto' onClick={() => pickContestType(item.title)}>Pilih</button>
                            </div>
                        </div>
                    ))}
                </div>
            </div>

            <div className='container py-5'>
                <div className='d-flex flex-wrap pb-3'>
                    <button className='btn btn-main px-5 py-3 m-2' onClick={onSave} disabled={state.choosenPackIdx == null || !state.contestType}>Simpan</button>
                    <button className='btn btn-main px-5 py-3 m-2' onClick={onContinue} disabled={state.choosenPackIdx == null|| !state.contestType}>Lanjut</button>
                </div>
                <Subfooter />
            </div>

        </LoadingOverlay>
    )
}

const mapStateToProps = state => {
    return {
        contest: state.contest.contest,
        loading: state.contest.loading,
        error: state.contest.error,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getContestById: (contestID, history) => dispatch(contestAction.getContestById(contestID, history)),
        updateContest: (contestID, payload) => dispatch(contestAction.updateContest(contestID, payload))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Pricing))