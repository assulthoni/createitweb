import Landing from './Landing'
import Page404 from './Page404'
import Homepage from './Homepage'
import DesignCategory from './DesignCategory/Index'
import BrowseContest from './Browse/Contest'
import BrowseProject from './Browse/Project'
import ProjectDashboard from './ProjectDashboard/Index'
import ContestDashboard from './ContestDashboard/Index'
import VerifyEmail from './VerifyEmail'
import ForgotPassword from './ForgotPassword'

import ProjectPayment from './ProjectPayment'

import {
    CreatorProfile, CreatorContestList, CreatorContestDashboard, CreatorPortofolio
} from './Creator/Index'

import {
    BriefContest, BriefProject, ContestReview, ProjectReview,
    Payment, Pricing, ProjectList, ContestList, ClientProfile,
} from './Client/Index'

export {
    ProjectPayment,
    ForgotPassword,
    VerifyEmail,
    ContestDashboard,
    ProjectDashboard,
    CreatorContestList,
    CreatorPortofolio,
    BrowseProject,
    BrowseContest,
    Homepage,
    ContestReview,
    ContestList,
    ProjectReview,
    CreatorProfile,
    ProjectList,
    BriefContest,
    BriefProject,
    Pricing,
    Page404,
    Landing,
    Payment,
    DesignCategory,
    ClientProfile
}