export const LOGO = {
    id:'logo',
    title: 'Logo & Branding Kit',
    subtitle: 'Mulai bisnismu dengan membuat logo yang mengesankan. Buat kesan pertama dengan logo yang menakjubkan dan tutup kesepakatan dengan Brand t yang tak terlupakan.',
    icon: 'Logo & branding kit.svg',
    iconBlue: 'Logo & branding kit-blue.svg',
    products: [
        {
            title: 'Logo',
            project: `Logo terbaik menjelaskan bisnismu dengan baik. Buat kesan pertama dengan logo yang menakjubkan. Unik, minimalis, modern, dan  mengesankan.
            Dapatkan logo terbaik untuk produkmu.`,
            contest: `Mulai kontes desain dan creator terbaik akan mebuat logo unik sesuai keinginanmu. Logo terbaik, ribuan ide unik, oleh creator terbaik, kamu yang tentukan! `,
            icon: 'Logo _ Branding Kit/logo.svg'
        },
        {
            title: 'Kartu Nama',
            project: `Tampilkan kartu nama yang mengesankan.
            Kesan pertamamu sudah baik. Ingat! kartu nama mu tak boleh terlupakan.
            Miliki kartu nama yang menarik, serahkan pada creator kami untuk menciptakan sesuai keinginan mu.`,
            contest: `Dapatkan kartu nama yang ideal dengan penawaran terbaik. Seluruh desainer kartu nama berpengalaman siap ikut kontes dan mengirimkan ide untukmu! 100% unik dan mengesankan.`,
            icon: 'Logo _ Branding Kit/kartu nama.svg'
        },
        {
            title: 'Brand Guidelines',
            project: `Creator profesional akan membuat Brand Guidelines yang dapat Anda bagikan dengan tim, mitra, dan desainer lainnya untuk memastikan konsistensi. Buat Brand Guidelines yang tepat dean jelas. Temukan desaigner yang kamu sukai dan bekerja samalah!`,
            icon: 'Logo _ Branding Kit/brand guidelines.svg'
        },
        {
            title: 'Stationery',
            contest: `---`,
            icon: 'Logo _ Branding Kit/stationery.svg'
        },
        {
            title: 'Notebook Cover',
            project: `Catatan ide-ide menarikmu tentu saja butuh sampul yang juga menarik. Diskusikan idemu bersama creator kami. `,
            contest: `Cari sampul terunik untuk buku catatanmu di antara hasil karya terbaik buatan creator kami`,
            icon: 'Logo _ Branding Kit/cover notebook.svg'
        },
    ]
}

export const CLOTH = {
    id:'cloth',
    title: 'Cloth & Merch',
    subtitle: 'Pastikan klien dan tim mu memakai dan mempromosikan brand mu. Buatlah seunik mungkin hingga tak terlupakan.',
    icon: 'Clothing & Merch.svg',
    iconBlue: 'Clothing & Merch-blue.svg',
    products: [
        {
            title: 'T-Shirt',
            project: `Orang-orang memakai kaos untuk melakukan segala hal. Desain yang bagus diinginkan oleh semua orang. Bekerja sama dengan creator profesional dan ciptakan T-shirt yang kau sukai!`,
            contest: `Pastikan t-shirt mu dikenakan dengan tepat. Temukan creator sprofesional untuk mewujudkan visi mu. Adakan kontes desain dan dapatkan ribuan ide dari desainer di seluruh dunia.`,
            icon: 'Clothing _ Merch/t-shirt.svg'
        },
        {
            title: 'Topi & Cap',
            project: `Topi & Cap khusus dirancang untuk mu agar lebih unggul. Buat topi kesukaan mu dan terus maju. Temukan desaigner yang kamu sukai dan bekerja samalah!`,
            contest: `Ribuan ide hanya untuk mu. Desainer di seluruh dunia menunggu terpilihnya untuk menang. mulai dari sekarang dan pilih desain topi favorit mu.`,
            icon: 'Clothing _ Merch/topi.svg'
        },
        {
            title: 'Tas & Totebag',
            project: `Aksesoris luar biasa ialah tas yang dirancang istimewa hanya untuk mu. Semua akan tertarik pada Tas & Totebag. Bekerjasama dengan desainer profesional dari sekarang.`,
            contest: `Ribuan ide hanya untuk mu. Desainer di seluruh dunia menunggu terpilih untuk menang. mulai dari sekarang dan pilih desain Tas & Totebag favorit mu.`,
            icon: 'Clothing _ Merch/tas.svg'
        },
    ]
}

export const BUSINESS = {
    id:'bussiness',
    title: 'Business & Marketing',
    subtitle: 'Promosikan brand mu dengan materi marketing yang berkualitas tinggi. Kamu bisa dapatkan yang terbaik disini.',
    icon: 'Business & Marketing.svg',
    iconBlue: 'Business & Marketing-blue.svg',
    products: [
        {
            title: 'Social Media Feeds',
            project: `Punya ide pemasaran yang hebat? Kreativitas mu tidak terkendala, dan demikian juga creators kami.`,
            contest: `Pastikan media sosial mu menjelaskan dengan tepat. Temukan creator profesional akan mewujudkan nya untuk mu. Adakan kontes desain dan dapatkan ribuan ide dari creator di seluruh dunia.`,
            icon: 'Business _ Marketing/social media feeds.svg'
        },
        {
            title: 'Brochure',
            project: `Promosikan brand mu dengan materi marketing yang berkualitas tinggi. Untuk iklan, kartu promo untuk semua Industri. Kamu bisa dapatkan yang terbaik dan cepat disini.`,
            contest: `Ribuan ide hanya untuk mu. Desainer di seluruh dunia menunggu terpilih untuk menang. Promosikan brand mu dengan materi marketing yang berkualitas tinggi.`,
            icon: 'Business _ Marketing/booklet.svg'
        },
        {
            title: 'Poster',
            project: `Poster singkat, padat, jelas, dan menarik perhatian ialah aset pemasaran bagi suatu bisnis untuk menyebarkan informasi. Kamu bisa dapatkan yang terbaik dan cepat disini.`,
            contest: `Creator mengajukan ide, dan kamu memilih desain terbaik. Desainer di seluruh dunia menunggu terpilih untuk menang.`,
            icon: 'Business _ Marketing/poster.svg'
        },
        {
            title: 'Banner',
            project: `Promosikan brand mu dengan materi marketing yang berkualitas tinggi. Kamu bisa dapatkan yang terbaik dan cepat disini.`,
            contest: `Ribuan ide hanya untuk mu. Creator di seluruh dunia menunggu terpilih untuk menang. Promosikan brand mu dengan materi marketing yang berkualitas tinggi.`,
            icon: 'Business _ Marketing/banner.svg'
        },
        {
            title: 'CV/Resume',
            project: `Ringkasan kualifikasi yang menarik. Buat kesan pertama dan citra yang menakjubkan. Unik, sederhana, modern, dan  mengesankan.`,
            contest: `Mulai kontes desain dan creator terbaik akan mebuat CV/Resume unik sesuai keinginanmu. hasil terbaik, ribuan ide unik, oleh creator terbaik, kamu yang tentukan!`,
            icon: 'Business _ Marketing/resume.svg'
        },
        {
            title: 'Menu',
            project: `Menu makanan mempermudah chef dan kustomer dalam menentukan makanan yang ingin dipesan. Desain menu makanan yang rapih dan tertata, Buat pelanggan merasa lebih mudah mendapatkan visualisasi menu yang ada.`,
            contest: `Mulai kontes desain dan creator terbaik akan mebuat desain menu yang unik sesuai keinginanmu. hasil terbaik, ribuan ide unik, oleh designer terbaik, kamu yang tentukan!`,
            icon: 'Business _ Marketing/menu.svg'
        },
        {
            title: 'Katalog',
            project: `Katalog mempermudah pelanggan dalam menentukan barang yang ingin dipesan. Desain katalog yang rapih dan tertata, Buat pelanggan merasa lebih mudah memilih yang diinginkan. Kamu bisa dapatkan yang terbaik disini.`,
            contest: `Ribuan ide hanya untuk mu. Creator di seluruh dunia menunggu terpilih untuk menang. Buat Katalog mu dengan desain yang berkualitas tinggi.`,
            icon: 'Business _ Marketing/catalog.svg'
        },
    ]
}

export const BOOK = {
    id:'book',
    title: 'Book & Magazine',
    subtitle: 'Berikan kesan indah pada tampilan dan setiap halaman. Selaraskan isi dengan tampilan agar lebih menarik dan buku mu akan tampak hebat.',
    icon: 'Book & Magazine.svg',
    iconBlue: 'Book & Magazine-blue.svg',
    products: [
        {
            title: 'Book Cover',
            project: `Misteri dalam buku disembunyikan pada Cover nya. Setiap halaman yang kau buat sudahlah bagus, saatnya tarik perhatian dengan sampul buku yang indah dan menakjubkan. Apapun isi didalamnya, sentuhan desain kami akan buat sampul bukumu bersinar.`,
            contest: `Misteri dalam buku disembunyikan pada Cover nya. Setiap halaman yang kau buat sudahlah bagus, saatnya tarik perhatian dengan sampul buku yang indah dan menakjubkan. Apapun isi didalamnya, sentuhan desain kami akan buat sampul bukumu bersinar.`,
            icon: 'Books _ Magazine/Book Cover.svg'
        },
        {
            title: 'Interior Book Design',
            project: `Desain interior buku yang berkesan dan bersinar. Beri kesan indah diantara setiap lembarnya maupun di luar. Ini semua yang kamu butuhkan untuk mengesankan penulis dan penerbit.`,
            contest: `Desain interior buku yang berkesan dan bersinar. Beri kesan indah diantara setiap lembarnya maupun di luar. Ini semua yang kamu butuhkan untuk mengesankan penulis dan penerbit.`,
            icon: 'Books _ Magazine/interior book design.svg'
        },
        {
            title: 'e-Book Cover',
            project: `---`,
            contest: `---`,
            icon: 'Books _ Magazine/ebook.svg'
        },
        {
            title: 'Magazine Cover',
            project: `Dapatkan cover majalah yang kamu sukai. Drama, fiksi, maupun fantasi didalamnya, sampul majalah tetap harus menarik. Tunjukan pada pembaca apa yang diharapkan saat mereka melihat ke dalam. Bekerja dengan desainer profesional di desain untuk membuat sampul luar biasa yang kamu dan pembacamu akan sukai.`,
            contest: `Dapatkan cover majalah yang kamu sukai. Drama, fiksi, maupun fantasi didalamnya, sampul majalah tetap harus menarik. Tunjukan pada pembaca apa yang diharapkan saat mereka melihat ke dalam. Bekerja dengan desainer profesional di desain untuk membuat sampul luar biasa yang kamu dan pembacamu akan sukai.`,
            icon: 'Books _ Magazine/magazine cover.svg'
        },
        {
            title: 'Book Layout',
            project: `Ada jauh lebih banyak hal untuk desain buku mu daripada sampulnya. Seorang penulis yang baik tahu bahwa ini semua tentang struktur. Desain tata letak buku, penyusunan huruf, dan lainnya! Buat lebih menarik dan buku mu akan tampak hebat dengan tata letak buku kustom yang rapih.`,
            contest: `Ada jauh lebih banyak hal untuk desain buku mu daripada sampulnya. Seorang penulis yang baik tahu bahwa ini semua tentang struktur. Desain tata letak buku, penyusunan huruf, dan lainnya! Buat lebih menarik dan buku mu akan tampak hebat dengan tata letak buku kustom yang rapih.`,
            icon: 'Books _ Magazine/book layout.svg'
        },
    ]
}

export const WEB = {
    id:'web',
    title: 'Web & App Design',
    subtitle: 'Setiap bisnis harus online. Dengan aplikasi yang didesain khusus, bisnis mu indah kemana pun pelanggan pergi. Dengan desain web terbaikbisnis mu akan lebih menakjubkan. Dapatkan desain yang bijaksana dan efektif yang dibuat oleh para profesional.',
    icon: 'Web & App Design.svg',
    iconBlue: 'Web & App Design-blue.svg',
    products: [
        {
            title: 'Halaman Web',
            project: `Jadikan halaman web paling populer menjadi milik mu dengan desain unik yang dirancang untuk spesifikasi Anda.`,
            contest: `Mulai Kontes Desain hari ini dan dapatkan tema desain yang sempurna dari desainer kami.`,
            icon: 'Web _ App design/halaman web.svg'
        },
        {
            title: 'Landing Page Design',
            project: `Buatlah Landing Page menjadi penjualan dengan desain yang indah dan jadikan bisnismu bersinar online.`,
            contest: `Mulai kontes dan creator kami akan membuat desain Landing Page berkualitas yang kamu sukai, dijamin.`,
            icon: 'Web _ App design/landing page.svg'
        },
        {
            title: 'App Design',
            project: `User-friendly untuk Aplikasi mu itu penting! nampak sederhana namun menarik jadikan aplikasi yang hebat dengan desain yang tepat.`,
            contest: `User-friendly untuk Aplikasi mu itu penting! nampak sederhana namun menarik jadikan aplikasi yang hebat dengan desain yang tepat.`,
            icon: 'Web _ App design/Desain aplikasi.svg'
        },
        {
            title: 'Social Media Design',
            project: `Creator kami membuat halaman media sosial yang sesuai dengan bisnis mu. Menarik, mencolok, informatif, dan tersampaikan adalah hal penting untuk media sosial mu. latar dan gambar memengaruhi peningkatan follower media sosialmu.`,
            contest: `Pastikan media sosial mu menampilkan dengan tepat. Temukan creator profesional akan mewujudkan nya untuk mu. Adakan kontes desain dan dapatkan ribuan ide dari creator di seluruh dunia.`,
            icon: 'Web _ App design/Desain social media.svg'
        },
        {
            title: 'Internet Ads',
            project: `Periklanan digital memungkinkan mu menargetkan audiens ideal. iklan yang hebat akan menarik perhatian mereka.  Promosikan brand mu dengan  desain yang berkualitas tinggi.`,
            contest: `Periklanan digital memungkinkan mu menargetkan audiens ideal. iklan yang hebat akan menarik perhatian mereka.  Promosikan brand mu dengan  desain yang berkualitas tinggi.`,
            icon: 'Web _ App design/internet ads.svg'
        },
        {
            title: 'Blog',
            project: `Tampilkan desain blog yang menarik dengan desain yang hebat.
            Buatlah pembaca bertahan dan tertarik dengan fokus pada kalimat yang tepat. 
            Sudah siap terhubung dengan pembacamu?`,
            contest: `Tampilkan desain blog yang menarik dengan desain yang hebat.
            Buatlah pembaca bertahan dan tertarik dengan fokus pada kalimat yang tepat. 
            Sudah siap terhubung dengan pembacamu?`,
            icon: 'Web _ App design/blog.svg'
        },
    ]
}

export const PACKAGING = {
    id:'packaging',
    title: 'Packaging & Label',
    subtitle: 'Kemasan yang bagus akan membuat produk mu menonjol. Desain label yang bagus, informatif, unik akan menarik hati pelanggan untuk produk mu.',
    icon: 'Packaging & Label.svg',
    iconBlue: 'Packaging & Label-blue.svg',
    products: [
        {
            title: 'Kemasan Packet',
            project: `Jika proyek mu tidak sesuai dengan salah satu kategori kami, itu tidak berarti desainer kami tidak bisa melakukannya. Pikirkan kemasan packet dengan kemasan yang dirancang secara profesional.`,
            contest: `Ribuan ide hanya untuk mu. Desainer di seluruh dunia menunggu giliran untuk menarik perhatian mu lewat sentuhan desain mereka.`,
            icon: 'Packaging _ Label/kemasan packet.svg'
        },
        {
            title: 'Tube',
            project: `Jika proyek mu tidak sesuai dengan salah satu kategori kami, itu tidak berarti desainer kami tidak bisa melakukannya. Dicari: Cara unik mempromosikan produk mu. Tentu saja, sediakan Tube spesial milikmu!`,
            contest: `Ribuan ide hanya untuk mu. Desainer di seluruh dunia menunggu giliran untuk menarik perhatian mu lewat sentuhan desain nya.`,
            icon: 'Packaging _ Label/kemasan tube.svg'
        },
        {
            title: 'Botol',
            project: `Dicari: Cara unik mempromosikan produk mu. Tentu saja, sediakan botol spesial milikmu!`,
            contest: `Ribuan ide hanya untuk mu. Desainer di seluruh dunia menunggu giliran untuk menarik perhatian mu lewat sentuhan desain nya.`,
            icon: 'Packaging _ Label/botol.svg'
        },
        {
            title: 'Kaleng',
            project: `Dicari: Cara unik mempromosikan produk mu. Tentu saja, sediakan Kaleng spesial milikmu!`,
            contest: `Ribuan ide hanya untuk mu. Desainer di seluruh dunia menunggu giliran untuk menarik perhatian mu lewat sentuhan desain nya.`,
            icon: 'Packaging _ Label/kaleng.svg'
        },
        {
            title: 'Box',
            project: `Dicari: Cara unik mempromosikan produk mu. Tentu saja, sediakan Box spesial milikmu!`,
            contest: `Ribuan ide hanya untuk mu. Desainer di seluruh dunia menunggu giliran untuk menarik perhatian mu lewat sentuhan desain nya. `,
            icon: 'Packaging _ Label/box.svg'
        },
    ]
}

export const MOTION = {
    id:'motion',
    title: 'Motion Graphic',
    subtitle: 'Ilustrasi yang menakjubkan menceritakan kisah lebih indah. Mural untuk dinding ruangan, gambar untuk buku,dan seni yang unik sangat menarik perhatian. Dapatkan ilustrasi grafis khusus dan desainer kami akan menciptakan sesuatu sesuai keinginan mu.',
    icon: 'Motion Graphic.svg',
    iconBlue: 'Motion Graphic-blue.svg',
    products: [
        {
            title: 'Company Profile Video',
            project: `Presentasikan Image dan bisnis perusahaan Kamu melalui video company profile yang menarik. Ceritakan perusahaanmu kepada customer melalui Video.`,
            contest: `Presentasikan Image dan bisnis perusahaan Kamu melalui video company profile yang menarik. Ceritakan perusahaanmu kepada customer melalui Video.`,
            icon: 'Motion Graphic/Company profile video.svg'
        },
        {
            title: 'Project Presentation Video',
            project: `Kami dapat membantu Anda untuk mempresentasikan project Anda kepada Stakeholder Anda melalui Video dengan menarik dan informatif.`,
            contest: `Kami dapat membantu Anda untuk mempresentasikan project Anda kepada Stakeholder Anda melalui Video dengan menarik dan informatif.`,
            icon: 'Motion Graphic/Project presentation video.svg'
        },
        {
            title: 'Social Media Ads',
            project: `Pengguna media sosial Indonesia tergolong terbanyak di Dunia dan dimana Iklan di media sosial lebih banyak diperhatikan daripada iklan pada media lainnya. Gapai perhatian dari pasar pengguna media sosial dengan Video yang menarik.`,
            contest: `Pengguna media sosial Indonesia tergolong terbanyak di Dunia dan dimana Iklan di media sosial lebih banyak diperhatikan daripada iklan pada media lainnya. Gapai perhatian dari pasar pengguna media sosial dengan Video yang menarik.`,
            icon: 'Motion Graphic/social media ads.svg'
        },
        {
            title: 'Explainer Video',
            project: `Jelaskan cara penggunaan dan fitur produk Anda dengan video penjelasan yang menarik dan profesional`,
            contest: `Jelaskan cara penggunaan dan fitur produk Anda dengan video penjelasan yang menarik dan profesional`,
            icon: 'Motion Graphic/explainer video.svg'
        },
    ]
}

export const ART = {
    id:'art',
    title: 'Art & Illustration',
    subtitle: 'Ilustrasi terbaik atau desainer grafis ada disini. Desainer yang tepat hanya dengan sekali klik. Beri tahu kami apa yang kamu cari dan kami akan mencocokkan dengan mitra desain yang sempurna.',
    icon: 'Art & Illustration.svg',
    iconBlue: 'Art & Illustration-blue.svg',
    products: [
        {
            title: 'Graphic Illustration',
            project: `--`,
            contest: `--`,
            icon: 'Art _ Illustration/graphic illustration.svg'
        },
        {
            title: 'Book Illustration',
            project: `Penulisan yang mengaggumkan akan tak menarik tanpa gambar yang konsisten. Bantu ceritakan kisah mu dengan ilustrasi khusus ang cocok.`,
            contest: `Penulisan yang mengaggumkan akan tak menarik tanpa gambar yang konsisten. Bantu ceritakan kisah mu dengan ilustrasi khusus ang cocok.`,
            icon: 'Art _ Illustration/Book illustration.svg'
        },
        {
            title: 'Invitation & Greeting Card',
            project: `Acara mu akan sangat menyenangkan. bersenang-senanglah dengan desain undangan spesial yang menunjukkan kepada tamu apa yang kau harapkan. Bagikan kartu ucapan khusus yang membuat pesan mu tak terlupakan. `,
            contest: `Acara mu akan sangat menyenangkan. bersenang-senanglah dengan desain undangan spesial yang menunjukkan kepada tamu apa yang kau harapkan. Bagikan kartu ucapan khusus yang membuat pesan mu tak terlupakan. `,
            icon: 'Art _ Illustration/invitation card.svg'
        },
    ]
}

export const CategoryList = [WEB, BOOK, BUSINESS, MOTION, LOGO, CLOTH, ART, PACKAGING]